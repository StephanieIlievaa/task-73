# A React Task
In this task will be updating the **title** DOM element with **useEffect**.
## Objective
* Checkout the dev branch.
* We must use the **useState** hook to sync the page title with the current state of the counter.
* We must use the **useEffect** hook to sync the page title with the current state of the counter.
* When everything is implemented, merge the dev branch into master before validating.
## Requirements
* The project starts with **npm run start**.
* Must use the **useEffect** hook with dependencies.
* The button text should look like that: **Count (1)** and the counter shoud start with 1.
* The title page should update with each counter state change and should look like that: **Count (1)**.
## Gotchas
More info about **useEffect** - https://reactjs.org/docs/hooks-effect.html